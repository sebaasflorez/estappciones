package com.example.sebastian.estappcion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EstacionEdicion extends AppCompatActivity {

    private RequestQueue mRequestQueue;
    private String baseUrl = "http://sebasflorez-estaciones.herokuapp.com/estaciones";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estacion_edicion);

        mRequestQueue = Volley.newRequestQueue(this);
        Intent intent = getIntent();

        String codigoEstacion = intent.getStringExtra(MapsActivity.CODIGO);
        consultarEstacion(codigoEstacion);
    }

    private void consultarEstacion(String codigo) {
        String url = baseUrl + "/" + codigo;
        HashMap<String, String> params = new HashMap<String, String>();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new JSONObject(params),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject estacion;
                        String codigoEstacion = "";
                        String nombre = "";
                        String fecha_actualizacion = "";
                        float precioGasolina = 0;
                        float precioDiesel = 0;

                        try {
                            estacion = response.getJSONObject("estacion");
                            codigoEstacion = estacion.get("codigoEstacion").toString();
                            nombre = estacion.get("nombre").toString();
                            precioGasolina = Float.parseFloat(estacion.get("precioGasolina").toString());
                            precioDiesel = Float.parseFloat(estacion.get("precioDiesel").toString());
                            fecha_actualizacion = estacion.get("fecha_actualizacion").toString();

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "ERROR: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }

                        TextView tEstacion = (TextView) findViewById(R.id.labelEstacion);
                        tEstacion.setText(nombre);

                        TextView tcodigo = (TextView) findViewById(R.id.textViewCodigo);
                        tcodigo.setText(codigoEstacion);

                        EditText tGasolina = (EditText) findViewById(R.id.editGasolina);
                        tGasolina.setText(String.valueOf(precioGasolina));

                        EditText tDiesel = (EditText) findViewById(R.id.editDiesel);
                        tDiesel.setText(String.valueOf(precioDiesel));

                        TextView tFecha = (TextView) findViewById(R.id.fechaActualizacion);
                        tFecha.setText(fecha_actualizacion);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int statusCode = -1;
                        NetworkResponse response = null;

                        if (error.networkResponse != null) {
                            response = error.networkResponse;
                            statusCode = response.statusCode;
                        }

                        String text = statusCode + ": " + response;
                        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                    }
                });

        mRequestQueue.add(request);
    }

    public void onRegresar(View view) {
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(intent);
        this.finish();

    }

    public void onActualizar(View view) {

        TextView tcodigo = (TextView) findViewById(R.id.textViewCodigo);
        EditText tGasolina = (EditText) findViewById(R.id.editGasolina);
        EditText tDiesel = (EditText) findViewById(R.id.editDiesel);

        String url = baseUrl;

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("codigoEstacion", tcodigo.getText().toString());
        params.put("precioGasolina", tGasolina.getText().toString());
        params.put("precioDiesel", tDiesel.getText().toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(params),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject estacion;
                        String cambio = "";
                        try {
                            estacion = response.getJSONObject("estacion");
                            cambio = response.getString("changes");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (cambio.equals("1"))
                            Toast.makeText(getApplicationContext(), "Actualizado", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(getApplicationContext(), "No Actualizado", Toast.LENGTH_LONG).show();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        int statusCode = -1;
                        NetworkResponse response = null;

                        if (error.networkResponse != null) {
                            response = error.networkResponse;
                            statusCode = response.statusCode;
                        }
                    }
                });

        mRequestQueue.add(request);

    }
}
