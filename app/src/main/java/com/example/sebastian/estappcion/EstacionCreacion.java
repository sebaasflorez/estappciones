package com.example.sebastian.estappcion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class EstacionCreacion extends AppCompatActivity {
    private String baseUrl = "http://sebasflorez-estaciones.herokuapp.com/estaciones";
    private RequestQueue mRequestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estacion_creacion);
        mRequestQueue = Volley.newRequestQueue(this);
    }

    public  void onRegresar(View view){
        Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void onCrear(View view){


        EditText tNombre = (EditText) findViewById(R.id.addNombre);
        EditText tGasolina = (EditText) findViewById(R.id.addGasolina);
        EditText tDiesel = (EditText) findViewById(R.id.addDiesel);
        EditText tLongitud = (EditText) findViewById(R.id.addLongitud);
        EditText tLatitud = (EditText) findViewById(R.id.addLatitud);
        TextView tusuario = (TextView) findViewById(R.id.addUsuario);

        if (tNombre.getText().toString().equals("")|| tGasolina.getText().toString().equals("") || tDiesel.getText().toString().equals("") || tLatitud.getText().toString().equals("") || tLongitud.getText().toString().equals("") ){
            Toast.makeText(getApplicationContext(),"Por favor, ingrese todos los datos",Toast.LENGTH_LONG).show();
            return;
        }

        String url = baseUrl;

        HashMap<String, String> params = new HashMap<String, String>();

        params.put("nombre",tNombre.getText().toString());
        params.put("precioGasolina",tGasolina.getText().toString());
        params.put("precioDiesel",tDiesel.getText().toString());
        params.put("longitud",tLongitud.getText().toString());
        params.put("latitud",tLatitud.getText().toString());
        params.put("codigoUsuario",tusuario.getText().toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        JSONObject estacion;
                        String cambio = "";
                        try
                        {
                            estacion = response.getJSONObject("estacion");
                            cambio = response.getString("changes");

                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        if (cambio.equals("1")) {
                            Toast.makeText(getApplicationContext(), "Estacion creada correctamente.", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(),MapsActivity.class);
                            startActivity(intent);
                            finish();

                        }else
                            Toast.makeText(getApplicationContext(),"Error al crear la estacion.",Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        int statusCode = -1;
                        NetworkResponse response = null;

                        if(error.networkResponse != null)
                        {
                            response = error.networkResponse;
                            statusCode = response.statusCode;
                        }
                    }
                });

        mRequestQueue.add(request);

    }

    public void onUbicacion(View view){

        Ubicacion u = new Ubicacion(this);
        u.getLocation();

        String latitud = String.valueOf(u.getLatitude());
        String longitud = String.valueOf(u.getLongitude());

        EditText tLongitud = (EditText) findViewById(R.id.addLongitud);
        EditText tLatitud = (EditText) findViewById(R.id.addLatitud);

        tLongitud.setText(longitud);
        tLatitud.setText(latitud);
    }
}
