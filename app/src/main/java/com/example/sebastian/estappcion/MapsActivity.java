package com.example.sebastian.estappcion;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pushbots.push.Pushbots;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,  OnMarkerClickListener {

    private GoogleMap mMap;
    public static String CODIGO = "com.example.sebasflorez.appestaciones";
    private String baseUrl      = "http://sebasflorez-estaciones.herokuapp.com/estaciones";
    private RequestQueue mRequestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Pushbot Notificaciones
        Pushbots.sharedInstance().init(this);
        //Pushbots.sharedInstance().setAlias("Sebas");

        mRequestQueue    = Volley.newRequestQueue(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        this.consultarServicioWeb();

        // Creacion de los Markadores

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // Ubicacion Actual

        Ubicacion u = new Ubicacion(this);
        u.getLocation();

        LatLng current = new LatLng(u.getLatitude(),u.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(current));


        googleMap.setOnMarkerClickListener((OnMarkerClickListener) this);

        // Evento al hacer click sobre la información
        mMap.setOnInfoWindowClickListener(
                new GoogleMap.OnInfoWindowClickListener(){
                    public void onInfoWindowClick(Marker marker){
                        Intent intent = new Intent(getApplicationContext(),EstacionEdicion.class);
                        intent.putExtra(MapsActivity.CODIGO,String.valueOf(String.valueOf(marker.getSnippet())));
                        startActivity(intent);
                        finish();
                    }
                }
        );    }

    // Evento al hacer click sobre el Marker
    public boolean onMarkerClick(Marker marker) {
        //Toast.makeText(getApplicationContext(),String.valueOf(marker.getPosition().latitude) ,Toast.LENGTH_LONG).show();
        return false;
    }

    public  void nuevaEstacion(View view){

        Intent intent = new Intent(getApplicationContext(),EstacionCreacion.class);
        startActivity(intent);
        this.finish();
    }

    private void  consultarServicioWeb()
    {
        HashMap<String, String> params = new HashMap<String, String>();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, this.baseUrl, new JSONObject(params),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response)
                    {

                        JSONArray estacionesArray  = new JSONArray();
                        try {

                            estacionesArray = response.getJSONArray("estaciones");

                            for (int i=0; i < estacionesArray.length(); i++){

                                mMap.addMarker(new MarkerOptions().position(new LatLng(
                                        Double.parseDouble(estacionesArray.getJSONObject(i).get("latitud").toString()),
                                        Double.parseDouble(estacionesArray.getJSONObject(i).get("longitud").toString())
                                        )).title(estacionesArray.getJSONObject(i).get("nombre").toString()
                                        ).snippet(estacionesArray.getJSONObject(i).get("codigoEstacion").toString()));
                            }


                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "ERROR: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        int statusCode = -1;
                        NetworkResponse response = null;

                        if(error.networkResponse != null)
                        {
                            response = error.networkResponse;
                            statusCode = response.statusCode;
                        }
                        String text = statusCode + ": " + response;
                        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                    }
                });

        mRequestQueue.add(request);
    }

}
